OF_ROOT = $(HOME)/openframeworks
APPNAME = vilain
# Version number comply with the Semantic Versioning http://semver.org
PROJECT_VERSION = 0.0.1
# PROJECT_ROOT = .
# PROJECT_EXTERNAL_SOURCE_PATHS =
PROJECT_ADDON_PATHS += $(PROJECT_ROOT)/addons/ofxAnimatable/src/
PROJECT_ADDON_PATHS += $(PROJECT_ROOT)/addons/ofxMeshWarp/src/
PROJECT_ADDON_PATHS += $(PROJECT_ROOT)/addons/ofxOscRouter/src/
PROJECT_EXCLUSIONS += $(PROJECT_ROOT)/addons/
PROJECT_EXCLUSIONS += $(PROJECT_ROOT)/addons/ofxAnimatable/example
PROJECT_EXCLUSIONS += $(PROJECT_ROOT)/addons/ofxMeshWarp/example/src
PROJECT_EXCLUSIONS += $(PROJECT_ROOT)/addons/ofxOscRouter/example/src
# PROJECT_LDFLAGS=-Wl,-rpath=./libs
PROJECT_DEFINES += PROG_NAME="\"$(APPNAME)\""
ifeq ($(findstring Debug,$(MAKECMDGOALS)),Debug)
PROJECT_DEFINES += DEBUG
endif

# PLATFORM_LIBRARIES +=
# PLATFORM_LIBRARY_SEARCH_PATHS +=
# PLATFORM_STATIC_LIBRARIES +=
# PLATFORM_PKG_CONFIG_LIBRARIES +=

# PROJECT_CFLAGS = -std=c++14
# PROJECT_OPTIMIZATION_CFLAGS_RELEASE =
# PROJECT_OPTIMIZATION_CFLAGS_DEBUG =
# PROJECT_CXX =g++
# PROJECT_CC =gcc

GETTEXT_MAINTAINER_EMAIL=dev@groolot.net
GETTEXT_COPYRIGHT_HOLDER=Grégory DAVID

# MAKEFILE_DEBUG=true
