#include "LayerProvider.h"

using namespace vilain;

LayerProvider::LayerProvider() :
    Provider<Layer>("layers")
{
}

LayerProvider::~LayerProvider()
{
}

void LayerProvider::processOscCommand(const std::string& command, const ofxOscMessage& m)
{
    ofLogVerbose(PROG_NAME, "LayerProvider received an OSC command: " + command);
    Provider::processOscCommand(command, m);
}
