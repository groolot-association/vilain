#include "ObjectProvider.h"

using namespace vilain;

ObjectProvider::ObjectProvider() :
    Provider<Object>("objects")
{
}

ObjectProvider::~ObjectProvider()
{
}

void ObjectProvider::processOscCommand(const std::string& command, const ofxOscMessage& m)
{
    ofLogVerbose(PROG_NAME, "ObjectProvider received an OSC command: " + command);
    Provider::processOscCommand(command, m);
};
