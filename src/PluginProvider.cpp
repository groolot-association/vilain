#include "PluginProvider.h"

using namespace vilain;

PluginProvider::PluginProvider() :
    Provider<Plugin>("plugins")
{
}

PluginProvider::~PluginProvider()
{
}

void PluginProvider::processOscCommand(const std::string& command, const ofxOscMessage& m)
{
    ofLogVerbose(PROG_NAME, "PluginProvider received an OSC command: " + command);
    Provider::processOscCommand(command, m);
}
