#pragma once
#ifndef SCENEPROVIDER_H
#define SCENEPROVIDER_H

#include "Providers.h"
#include "Singleton.h"
#include "Scene.h"

namespace vilain
{
/**
 * \addtogroup providers
 * \{
 */

/**
 * \class SceneProvider
 * \brief Instanciation class of a Provider<Scene>
 *
 * Additionnal methods and properties belong to it.
 * \copyright GNU Public License
 */
class SceneProvider :
    public Singleton<SceneProvider>,
    public Provider<Scene>
{
    friend Singleton<SceneProvider>;

public:
    /**
     * \brief Access to the current activated scene identifier
     * @return The identifier string of the current scene
     */
    std::string getCurrent();

    /**
     * \brief Access to the next scene identifier defined as activable but not activated yet
     * @return The identifier string of the next scene
     */
    std::string getNext();

    /**
     * \brief Access to the previously activated scene identifier
     * @return The identifier string of the previous scene
     */
    std::string getPrevious();

    /**
     * \brief Define the next activable scene
     * Activation is done by a call to goToNext.
     *
     * @param _nextSceneName Next scene identifier name
     */
    void setNext(const std::string& _nextSceneName);

    /**
     * \brief Activate the next scene defined with setNext
     * If the next scene identifier do not exists in the scene collection,
     * nothing is done and the current scene continues to be activated as normal
     *
     */
    void goToNext();

    /**
     * \brief Activate the previously activated scene
     * If the penultimate scene does not exist anymore, try the antepenultimate,
     * and so on until finding an activable scene.
     * If no previous scene is activable, then DO NOT DO anything.
     */
    void goToPrevious();

    /**
     * \brief OSC Message processing callback
     *
     * This is the list of OSC message that can be interpreted by the processor:
     * - ../scenes/exampleToRemove ifs varInt varFloat varString
     *
     * \param command OSC command string associated with this ofxOscRouterNode
     * \param m Complete ofxOscMessage container, with arguments and OSC packet informations
     *
     * \todo Write documentation for each OSC command available
     */
    void processOscCommand(const std::string& command, const ofxOscMessage& m);

protected:

private:
    /**
     * \brief Private constructor, only called by the Singleton::instance function
     */
    SceneProvider();

    /**
     * \brief Private destructor
     */
    ~SceneProvider();

    std::string next_scene; ///< String container to store next scene identifier
    std::stack<std::string> history; ///< Stack to store all the switched scenes history
};
/**
 * \}
 */
};
#endif // SCENEPROVIDER_H
