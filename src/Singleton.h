#pragma once
#ifndef SINGLETON_H
#define SINGLETON_H
#include <cstddef>

/**
 * \addtogroup core
 * \{
 */

/**
 * \class Singleton
 * \brief Helper template class to implement singleton
 * \copyright GNU Public License
 */
template <class T>
class Singleton
{
private:
    /**
     * \brief Private copy constructor to avoid copy
     */
    Singleton(Singleton const&) {};

    /**
     * \brief Disable the copy by privating operator =
     */
    Singleton& operator=(Singleton const&) {};

protected:
    static T * _singleton; ///< Static reference to itself

    /**
     * \brief Protected constructor to allow construction by template instance
     */
    Singleton()
    {
        _singleton = static_cast <T *>(this);
    };

    /**
     * \brief Protected destructor
     */
    ~Singleton() {};

public:
    /**
     * \brief Access to the instance of the singleton, or allocate it if not existing
     * @return Pointer to itself
     */
    static T * instance()
    {
        if(_singleton == NULL)
        {
            _singleton = new T;
        }

        return (static_cast<T *>(_singleton));
    };

    /**
     * \brief Release the content of the singleton
     *
     * \warning When released, the content is totally freed and lost
     */
    static void release()
    {
        if(_singleton)
        {
            delete _singleton;
        }

        _singleton = NULL;
    };
};
#endif // SINGLETON_H

template <class T>
T * Singleton<T>::_singleton = NULL;

/**
 * \}
 */
