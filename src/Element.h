#pragma once
#ifndef ELEMENT_H
#define ELEMENT_H

#include "vilain.h"
#include "ofxOscRouterNode.h"
#include "Plugin.h"

namespace vilain
{
/**
 * \defgroup elements Elements and such things
 *
 * Everything known to be an Element in the system
 * \{
 */

/**
 * \class Element
 * \brief Mother class for things that are like an element in the program
 *
 * It is not really clearly defined what is an Element. You can take it as
 * an helper class that encapsulate a name and which is OSC-able.
 * Whatever thing that have to be OSC controllable and compound in a collection
 * should be an Element derivated class.
 * \copyright GNU Public License
 */
class Element :
    public ofxOscRouterNode
{
public:
    /**
     * \brief Element's constructor that need a name
     * @param _name Element's name that is used to identify the element in the
     * OSC tree and inside elements collection.
     */
    Element(std::string _name) : ofxOscRouterNode(_name), name(_name) {};

    /**
     * \brief Destructor
     */
    virtual ~Element() {};

    /**
     * \brief Abstract method that should draw the element on screen or framebuffer
     *
     * We suppose that each Element's subclass wants to be drawn somewhere, then
     * this method is purely virtual to enforce this.
     * If an element do not need to be drawn, you just have to define the inherited
     * method with an empty content.
     * @code
     * class Something : public Element {
     *    Something();
     *    ~Something();
     *    void draw() {}; // empty content, but defined method ; compiler do not cry
     * }
     * @endcode
     */
    virtual void draw() = 0;

    /**
     * \brief OSC Message processing callback
     *
     * This is the list of OSC messages that can be interpreted by the processor:
     * - ../elementName/exampleToRemove ifs varInt varFloat varString
     *
     * \param command OSC command string associated with this ofxOscRouterNode
     * \param m Complete ofxOscMessage container, with arguments and OSC packet informations
     *
     * \todo Write documentation for each OSC command available
     */
    virtual void processOscCommand(const std::string& command, const ofxOscMessage& m) = 0;

    /**
     * \brief As draw() method, but for the updating mechanism
     *
     * Have to be defined in subclass
     */
    virtual void update() = 0;

    /**
     * \brief get the node name of the Element
     * @return The std::string node name
     */
    const std::string& getNodeName()
    {
        return name;
    };

protected:
    std::string name; ///< Element's name that identify itself
    std::map<std::uint16_t, Plugin> plugins; ///< Ordered list of plugins to be applied

    /**
     * \brief Define the new name of the Element
     * @param _name Name to be newly set
     * @return True if the new name is correct and set, False instead.
     */
    bool setNodeName(std::string _name)
    {
        // TODO: we have to validate name instead of direct applying
        name = _name;
        return true;
    }

    /**
     * \brief As draw(), abstract method that compute the plugin chain for the
     * current Element
     *
     * HAve to be defined in subclass
     */
    virtual void computePlugins() = 0;

};
/**
 * \}
 */
};
#endif // ELEMENT_H
