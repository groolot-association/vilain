#include "Configuration.h"

using namespace vilain;

/// \brief Private constructor to assume singleton pattern
///
Configuration::Configuration() :
    ofxOscRouterNode("configure"),
    _oscRootNodeName(PROG_NAME),
    _oscPort(OSC_LISTENER_PORT)
{
    _styleStandard = new Style("style");
    addOscChild(_styleStandard);
}

/// \brief Destructor
///
Configuration::~Configuration()
{
}

/// \brief OSC Message processing callback
///
/// \param command const string& OSC command associated with this ofxOscRouterNode
/// \param m const ofxOscMessage& Complete ofxOscMessage container, with arguments and OSC packet informations
/// \return void
///
/// \todo Write documentation for each OSC command available
///
void Configuration::processOscCommand(const std::string& command, const ofxOscMessage& m)
{
    ofLogVerbose(PROG_NAME, "Received OSC address /" + getFirstOscNodeAlias() + "/" + command);
}

/// \brief
///
/// \return std::uint16_t
///
///
std::uint16_t Configuration::oscPort()
{
    return _oscPort;
}

/// \brief
///
/// \return std::string
///
///
std::string Configuration::oscRootNodeName()
{
    return _oscRootNodeName;
}

/// \brief
///
///
Style Configuration::getStyle()
{
    return (*_styleStandard);
}

/// \brief
///
/// \param _style ofStyle
void Configuration::setStyle(Style _style)
{
    *_styleStandard = _style;
}
