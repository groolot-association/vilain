#include "Object.h"

using namespace vilain;

Object::Object(std::string _name, unsigned char _key) :
    Element(_name),
    bDrawable(false),
    bAnimable(true),
    activeKey(_key)
{
    alpha.reset(0);
    alpha.setDuration(1);
    alpha.setRepeatType(PLAY_ONCE);
    alpha.setCurve(EASE_IN_EASE_OUT);
    initializeOscMethods();
}

Object::~Object()
{
}

void Object::initializeOscMethods()
{
    addOscMethod("drawable");
    addOscMethod("animable");
    addOscMethod("alpha");
    addOscMethod("show");
    addOscMethod("hide");
    addOscMethod("duration");
}

void Object::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "drawable"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            Drawable(getArgAsBoolUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "animable"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            Animable(getArgAsBoolUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "show"))
    {
        if(validateOscSignature("([TFif][if]?[if]?)", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                if(m.getNumArgs() >= 2)
                {
                    if(m.getNumArgs() == 3)
                    {
                        alpha.setDuration(getArgAsFloatUnchecked(m, 2));
                    }

                    show(getArgAsFloatUnchecked(m, 1));
                }
                else
                {
                    show();
                }
            }
            else
            {
                if(m.getNumArgs() == 2)
                {
                    alpha.setDuration(getArgAsFloatUnchecked(m, 1));
                }

                show();
            }
        }
    }
    else if(isMatch(command, "hide"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                hide();
            }
        }
    }
    else if(isMatch(command, "alpha"))
    {
        if(validateOscSignature("([if])", m))
        {
            Alpha(getArgAsFloatUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "duration"))
    {
        if(validateOscSignature("([if])", m))
        {
            alpha.setDuration(getArgAsFloatUnchecked(m, 0));
        }
    }
}

void Object::update()
{
    if(Animable())
    {
        float _dt = 1.0f / ofGetTargetFrameRate();
        alpha.update(_dt);
    }
}

void Object::draw()
{
    if(Drawable())
    {
        ofDrawBitmapString(ofxOscRouterNode::getFirstOscNodeAlias(), ofGetWidth() / 2, ofGetHeight() / 2);
    }
}

void Object::computePlugins()
{
}

void Object::show(float _maximumVisibility)
{
    alpha.animateTo(_maximumVisibility);
}

void Object::hide()
{
    alpha.animateTo(0);
}

void Object::Alpha(float _valAlpha)
{
    alpha.reset(ofClamp(_valAlpha, 0, 1));
}

float Object::Alpha()
{
    return alpha.val();
}

bool Object::Drawable()
{
    return bDrawable;
}

void Object::Drawable(bool _val)
{
    bDrawable = _val;
}

void Object::Toggle()
{
    bDrawable = !bDrawable;
}

void Object::Animable(bool _val)
{
    bAnimable = _val;
}

bool Object::Animable()
{
    return bAnimable;
}

const char Object::Key()
{
    return activeKey;
}

void Object::Key(char _key)
{
    activeKey = _key;
}
