#include "SceneProvider.h"

using namespace vilain;

SceneProvider::SceneProvider() :
    Provider<Scene>("scenes")
{
}

SceneProvider::~SceneProvider()
{
}

std::string SceneProvider::getCurrent()
{
    return "getCurrent";
}

std::string SceneProvider::getNext()
{
    return next_scene;
}

std::string SceneProvider::getPrevious()
{
    return "getPrevious";
}

void SceneProvider::setNext(const std::string& _nextSceneName)
{
}

void SceneProvider::goToNext()
{
}

void SceneProvider::goToPrevious()
{
}

void SceneProvider::processOscCommand(const std::string& command, const ofxOscMessage& m)
{
    ofLogVerbose(PROG_NAME, "SceneProvider received an OSC command: " + command);
    Provider::processOscCommand(command, m);
}
