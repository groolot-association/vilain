#pragma once
#ifndef PLUGINPROVIDER_H
#define PLUGINPROVIDER_H

#include "Providers.h"
#include "Singleton.h"
#include "Plugin.h"

namespace vilain
{
/**
 * \addtogroup providers
 * \{
 */

/**
 * \class PluginProvider
 * \brief Instanciation class of a Provider<Plugin>
 *
 * Additionnal methods and properties belong to it.
 * \copyright GNU Public License
 */
class PluginProvider :
    public Singleton<PluginProvider>,
    public Provider<Plugin>
{
    friend Singleton<PluginProvider>;

public:
    /**
     * \brief OSC Message processing callback
     *
     * This is the list of OSC message that can be interpreted by the processor:
     * - ../plugins/exampleToRemove ifs varInt varFloat varString
     *
     * \param command OSC command string associated with this ofxOscRouterNode
     * \param m Complete ofxOscMessage container, with arguments and OSC packet informations
     *
     * \todo Write documentation for each OSC command available
     */
    void processOscCommand(const std::string& command, const ofxOscMessage& m);

protected:

private:
    /**
     * \brief Private constructor, only called by the Singleton::instance function
     */
    PluginProvider();

    /**
     * \brief Private destructor
     */
    ~PluginProvider();
};
/**
 * \}
 */
};
#endif // PLUGINPROVIDER_H
