#include "Core.h"
#include "Object.h"

using namespace vilain;

Core::Core():
    ofBaseApp(),
    ofxOscRouter(Configuration::instance()->oscRootNodeName(), Configuration::instance()->oscPort()),
    bDebug(false),
    bBlackOut(false)
{
#ifdef DEBUG
    bDebug = true;
#endif // DEBUG
    configurationMgr = Configuration::instance();
    scenesMgr = SceneProvider::instance();
    layersMgr = LayerProvider::instance();
    objectsMgr = ObjectProvider::instance();
    pluginsMgr = PluginProvider::instance();

    oscSender.setup("127.0.0.1", configurationMgr->oscPort());

    addOscChild(configurationMgr);
    addOscChild(scenesMgr);
    addOscChild(layersMgr);
    addOscChild(objectsMgr);
    addOscChild(pluginsMgr);

    addOscMethod("exit");
    addOscMethod("debug");
    addOscMethod("blackout");
}

Core::~Core()
{
}

void Core::setup()
{
    ofLogVerbose(PROG_NAME, "Dump OSC schema:\n" + schemaToString());
    ofSetFrameRate(60);
}

void Core::update()
{
    ofxOscRouter::update();
}

void Core::draw()
{
    ofSetStyle(configurationMgr->getStyle());

    if(bBlackOut)
    {
        ofDrawBitmapString("BLACKOUT", 10, 10);
    }
    else
    {
        // Dessiner
    }

    if(bDebug) // Keep it last to always draw DEBUG stuff on top of everything
    {
        ofDrawBitmapString("DEBUG", 100, 100);
    }
}

void Core::exit()
{
    ofLogVerbose(PROG_NAME, "exiting...");
}

void Core::processOscCommand(const std::string& command, const ofxOscMessage& m)
{
    ofLogVerbose(PROG_NAME, "Received OSC address /" + ofToString(PROG_NAME) + "/" + command);

    if(isMatch(command, "exit"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                ofExit();
            }
        }
    }
    else if(isMatch(command, "debug"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            bDebug = getArgAsBoolUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "blackout"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            bBlackOut = getArgAsBoolUnchecked(m, 0);
        }
    }
}

void Core::keyPressed(int key)
{
    if(key == OF_KEY_RETURN)
    {
    }
    else if(key == 'D')
    {
        oscMessage.clear();
        oscMessage.setAddress("/" + configurationMgr->oscRootNodeName() + "/debug");
        oscMessage.addBoolArg(!bDebug);
        oscSender.sendMessage(oscMessage);
    }
    else if(key == ' ')
    {
        oscMessage.clear();
        oscMessage.setAddress("/" + configurationMgr->oscRootNodeName() + "/blackout");
        oscMessage.addIntArg(!bBlackOut);
        oscSender.sendMessage(oscMessage);
    }
}

void Core::keyReleased(int key)
{
}

void Core::mouseMoved(int x, int y)
{
}

void Core::mousePressed(int x, int y, int button)
{
    oscMessage.clear();
    oscMessage.setAddress("/" + configurationMgr->oscRootNodeName() + "/blackout");
    oscMessage.addBoolArg(true);
    oscSender.sendMessage(oscMessage);
}

void Core::mouseReleased(int x, int y, int button)
{
    oscMessage.clear();
    oscMessage.setAddress("/" + configurationMgr->oscRootNodeName() + "/blackout");
    oscMessage.addBoolArg(false);
    oscSender.sendMessage(oscMessage);
}

void Core::mouseScrolled(int x, int y, float scrollX, float scrollY)
{
}

void Core::windowResized(int w, int h)
{
}
