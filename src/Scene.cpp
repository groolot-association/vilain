#include "Scene.h"

using namespace vilain;

Scene::Scene(std::string _name) :
    Element(_name)
{
}

Scene::~Scene()
{
}

void Scene::computePlugins()
{
}
void Scene::draw()
{
}
void Scene::processOscCommand(const std::string& command, const ofxOscMessage& m)
{
}
void Scene::update()
{
}
