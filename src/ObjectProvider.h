#pragma once
#ifndef OBJECTPROVIDER_H
#define OBJECTPROVIDER_H

#include "Providers.h"
#include "Singleton.h"
#include "Object.h"

namespace vilain
{
/**
 * \addtogroup providers
 * \{
 */

/**
 * \class ObjectProvider
 * \brief Instanciation class of a Provider<Object>
 *
 * Additionnal methods and properties belong to it.
 * \copyright GNU Public License
 */
class ObjectProvider :
    public Singleton<ObjectProvider>,
    public Provider<Object>
{
    friend Singleton<ObjectProvider>;

public:
    /**
     * \brief OSC Message processing callback
     *
     * This is the list of OSC message that can be interpreted by the processor:
     * - ../objects/exampleToRemove ifs varInt varFloat varString
     *
     * \param command OSC command string associated with this ofxOscRouterNode
     * \param m Complete ofxOscMessage container, with arguments and OSC packet informations
     *
     * \todo Write documentation for each OSC command available
     */
    void processOscCommand(const std::string& command, const ofxOscMessage& m);

protected:

private:
    /**
     * \brief Private constructor, only called by the Singleton::instance function
     */
    ObjectProvider();

    /**
     * \brief Private destructor
     */
    ~ObjectProvider();
};
/**
 * \}
 */
};
#endif // OBJECTPROVIDER_H
