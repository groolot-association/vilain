#pragma once

//---------- openFrameworks
#include <ofMain.h>

//---------- i18n
#include <libintl.h>
#define _(String) gettext(String)
#define _N(String) String

//---------- Globals
#include "Singleton.h"
#include "Constants.h"
