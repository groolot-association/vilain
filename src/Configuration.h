#pragma once
#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include "vilain.h"
#include "Style.h"
#include <ofxOscRouterNode.h>

namespace vilain
{
class Configuration :
    public Singleton<Configuration>,
    public ofxOscRouterNode
{
    friend class Singleton<Configuration>;

public:
    void processOscCommand(const std::string& command, const ofxOscMessage& m);

    std::uint16_t oscPort();
    std::string oscRootNodeName();
    std::string nodeName();
    Style getStyle();
    void setStyle(Style _style);

private:
    Configuration();
    ~Configuration();

    std::string _oscRootNodeName;
    std::uint16_t _oscPort;
    Style * _styleStandard;
};
};
#endif // CONFIGURATION_H
