#include "Layer.h"

using namespace vilain;

Layer::Layer(std::string _name):
    Element(_name)
{
    objectsMgr = ObjectProvider::instance();
}

Layer::~Layer()
{
}

void Layer::computePlugins()
{
}
void Layer::draw()
{
}
void Layer::processOscCommand(const std::string& command, const ofxOscMessage& m)
{
}
void Layer::update()
{
}

void Layer::associateObject(const std::string& _objectName)
{
    ofLogVerbose(PROG_NAME, "Try to associate '" + _objectName + "' object with '" + getNodeName() + "' layer");

    if(objects.count(_objectName) == 0)
    {
        objects[_objectName] = objectsMgr->getPtr(_objectName);
        addOscChild(objects[_objectName]);
        ofLogVerbose(PROG_NAME, "Object named '" + _objectName + "' has been associated correctly with '" + getNodeName() + "' layer");
    }
    else
    {
        ofLogWarning(PROG_NAME, "Object named '" + _objectName + "' has already been associated with '" + getNodeName() + "' layer");
    }
}
