#pragma once
#ifndef PLUGIN_H
#define PLUGIN_H

#include "ofxOscRouterNode.h"

namespace vilain
{
/**
 * \defgroup plugins Plugin system
 *
 * Plugin system's collaborators
 * \{
 */

/**
 * \class Plugin
 * \brief Mother class for all plugins
 *
 * Once you want to develop a new plugin, you have to inherit from this class.
 * \copyright GNU Public License
 */
class Plugin :
    public ofxOscRouterNode
{
public:
    /**
     * \brief Named constructor
     * @param _name Plugin name that identify itself
     */
    Plugin(std::string _name);

    /**
     * \brief Destructor
     */
    virtual ~Plugin();

    /**
     * \brief Drawing method
     *
     * Each plugin have to implement this method to achieve drawing
     */
    virtual void draw() {};

    /**
     * \brief Update method for animations
     */
    virtual void update() {};

    /**
     * \brief OSC Message processing callback
     *
     * This is the list of OSC messages that can be interpreted by the processor:
     * - ../pluginName/exampleToRemove ifs varInt varFloat varString
     *
     * \param command OSC command string associated with this ofxOscRouterNode
     * \param m Complete ofxOscMessage container, with arguments and OSC packet informations
     *
     * \todo Write documentation for each OSC command available
     */
    virtual void processOscCommand(const std::string& command, const ofxOscMessage& m) {};

};
/**
 * \}
 */
};
#endif // PLUGIN_H
