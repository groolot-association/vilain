#pragma once
#ifndef LAYERPROVIDER_H
#define LAYERPROVIDER_H

#include "Providers.h"
#include "Singleton.h"
#include "Layer.h"

namespace vilain
{
/**
 * \addtogroup providers
 * \{
 */

/**
 * \class LayerProvider
 * \brief Instanciation class of a Provider<Layer>
 *
 * Additionnal methods and properties belong to it.
 * \copyright GNU Public License
 */
class LayerProvider :
    public Singleton<LayerProvider>,
    public Provider<Layer>
{
    friend Singleton<LayerProvider>; ///< Give access to all members, private constructor included, to Singleton class

public:
    /**
     * \brief OSC Message processing callback
     *
     * This is the list of OSC message that can be interpreted by the processor:
     * - ../layers/exampleToRemove ifs varInt varFloat varString
     *
     * \param command OSC command string associated with this ofxOscRouterNode
     * \param m Complete ofxOscMessage container, with arguments and OSC packet informations
     *
     * \todo Write documentation for each OSC command available
     */
    void processOscCommand(const std::string& command, const ofxOscMessage& m);

protected:

private:
    /**
     * \brief Private constructor, only called by the Singleton::instance function
    */
    LayerProvider();

    /**
     * \brief Private destructor
     */
    ~LayerProvider();
};
/**
 * \}
 */
};
#endif // LAYERPROVIDER_H
