#pragma once
#ifndef SCENE_H
#define SCENE_H

#include "Element.h"
#include "Layer.h"

namespace vilain
{
/**
 * \addtogroup elements
 * \{
 */

/**
 * \class Scene
 * \brief A scene is a Layer container that can be drawn on framebuffer or screen
 *
 * \copyright GNU Public License
 */
class Scene :
    public Element
{
public:
    /**
     * \brief Named constructor
     * @param _name Scene name that identify itself
     */
    Scene(std::string _name);

    /**
     * \brief Destructor
     */
    virtual ~Scene();

    /**
     * \brief Implement the mother's class abstract method Element::draw()
     *
     * This method compute the drawing things on framebuffer and on screen
     */
    virtual void draw();

    /**
     * \brief OSC Message processing callback
     *
     * This is the list of OSC messages that can be interpreted by the processor:
     * - ../sceneName/exampleToRemove ifs varInt varFloat varString
     *
     * \param command OSC command string associated with this ofxOscRouterNode
     * \param m Complete ofxOscMessage container, with arguments and OSC packet informations
     *
     * \todo Write documentation for each OSC command available
     */
    virtual void processOscCommand(const std::string& command, const ofxOscMessage& m);

    /**
     * \brief Updating mechanism to permit animations
     */
    virtual void update();

    std::map<std::string, Layer*> layers;

private:
    /**
     * \brief Compute the plugin chain for the current Scene
     *
     * Each plugin is applied once in the plugin stack order
     */
    virtual void computePlugins();
};
/**
 * \}
 */
};
#endif // SCENE_H
