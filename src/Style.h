#pragma once
#ifndef STYLE_H
#define STYLE_H

#include "vilain.h"
#include <ofxOscRouterNode.h>

namespace vilain
{
class Style :
    public ofStyle,
    public ofxOscRouterNode
{
public:
    Style(std::string _name);
    ~Style();

public:
    void processOscCommand(const std::string& command, const ofxOscMessage& m);
};
};
#endif // STYLE_H
