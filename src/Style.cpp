#include "Style.h"

using namespace vilain;

/**
 * \brief Constructor
 *
 * Default style sets a white foreground on a black background with filling
 *
 * @param _name OSC node name of the style
 */
Style::Style(std::string _name) :
    ofStyle(),
    ofxOscRouterNode(_name)
{
    bFill                 = true;
    lineWidth             = 1.0f;
    curveResolution       = 10;
    circleResolution      = 80;
    smoothing             = true;
    color.set(255);
    bgColor.set(0);
    drawBitmapMode        = OF_BITMAPMODE_SIMPLE;

    addOscMethod("color");
    addOscMethod("bgColor");
    addOscMethod("polyMode");
    addOscMethod("rectMode");
    addOscMethod("bFill");
    addOscMethod("drawBitmapMode");
    addOscMethod("blendingMode");
    addOscMethod("smoothing");
    addOscMethod("circleResolution");
    addOscMethod("sphereResolution");
    addOscMethod("curveResolution");
    addOscMethod("lineWidth");
}

/**
 * \brief Destructor
 */
Style::~Style()
{
    while(hasParents())
    {
        removeOscParent(getFirstOscParent());
    }
}

/**
 * \brief
 * @param command
 * @param m
 */
void Style::processOscCommand(const std::string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "color"))
    {
        if(validateOscSignature("([if]{4})", m))
        {
            color.set(getArgAsIntUnchecked(m, 0),
                      getArgAsIntUnchecked(m, 1),
                      getArgAsIntUnchecked(m, 2),
                      getArgAsIntUnchecked(m, 3)
                     );
        }
    }
    else if(isMatch(command, "bgColor"))
    {
        if(validateOscSignature("([if]{3})", m))
        {
            bgColor.set(getArgAsIntUnchecked(m, 0),
                        getArgAsIntUnchecked(m, 1),
                        getArgAsIntUnchecked(m, 2)
                       );
        }
    }
    else if(isMatch(command, "polyMode"))
    {
        if(validateOscSignature("([sif])", m))
        {
            if(
                m.getArgType(0) == OFXOSC_TYPE_INT32 or
                m.getArgType(0) == OFXOSC_TYPE_FLOAT
            )
            {
                polyMode = (ofPolyWindingMode)getArgAsIntUnchecked(m, 0);
            }
            else if(m.getArgType(0) == OFXOSC_TYPE_STRING)
            {
                std::string _arg = getArgAsStringUnchecked(m, 0);

                if("odd" == _arg)
                    polyMode = OF_POLY_WINDING_ODD;
                else if("nonzero")
                    polyMode = OF_POLY_WINDING_NONZERO;
                else if("positive")
                    polyMode = OF_POLY_WINDING_POSITIVE;
                else if("negative")
                    polyMode = OF_POLY_WINDING_NEGATIVE;
                else if("abs_geq_two")
                    polyMode = OF_POLY_WINDING_ABS_GEQ_TWO;
            }
        }
    }
    else if(isMatch(command, "rectMode"))
    {
        if(validateOscSignature("([sif])", m))
        {
            if(
                m.getArgType(0) == OFXOSC_TYPE_INT32 or
                m.getArgType(0) == OFXOSC_TYPE_FLOAT
            )
            {
                rectMode = (ofRectMode)getArgAsIntUnchecked(m, 0);
            }
            else if(m.getArgType(0) == OFXOSC_TYPE_STRING)
            {
                std::string _arg = getArgAsStringUnchecked(m, 0);

                if("corner")
                    rectMode = OF_RECTMODE_CORNER;
                else if("center")
                    rectMode = OF_RECTMODE_CENTER;
            }
        }
    }
    else if(isMatch(command, "bFill"))
    {
        if(validateOscSignature("([TFifs])", m))
        {
            bFill = getArgAsBoolUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "drawBitmapMode"))
    {
        if(validateOscSignature("([sif])", m))
        {
            if(
                m.getArgType(0) == OFXOSC_TYPE_INT32 or
                m.getArgType(0) == OFXOSC_TYPE_FLOAT
            )
            {
                drawBitmapMode = (ofDrawBitmapMode)getArgAsIntUnchecked(m, 0);
            }
            else if(m.getArgType(0) == OFXOSC_TYPE_STRING)
            {
                std::string _arg = getArgAsStringUnchecked(m, 0);

                if("simple")
                    drawBitmapMode = OF_BITMAPMODE_SIMPLE;
                else if("screen")
                    drawBitmapMode = OF_BITMAPMODE_SCREEN;
                else if("viewport")
                    drawBitmapMode = OF_BITMAPMODE_VIEWPORT;
                else if("model")
                    drawBitmapMode = OF_BITMAPMODE_MODEL;
                else if("billboard")
                    drawBitmapMode = OF_BITMAPMODE_MODEL_BILLBOARD;
            }
        }
    }
    else if(isMatch(command, "blendingMode"))
    {
        if(validateOscSignature("([sif])", m))
        {
            if(
                m.getArgType(0) == OFXOSC_TYPE_INT32 or
                m.getArgType(0) == OFXOSC_TYPE_FLOAT
            )
            {
                blendingMode = (ofBlendMode)getArgAsIntUnchecked(m, 0);
            }
            else if(m.getArgType(0) == OFXOSC_TYPE_STRING)
            {
                std::string _arg = getArgAsStringUnchecked(m, 0);

                if("disabled")
                    blendingMode = OF_BLENDMODE_DISABLED;
                else if("alpha")
                    blendingMode = OF_BLENDMODE_ALPHA;
                else if("add")
                    blendingMode = OF_BLENDMODE_ADD;
                else if("substract")
                    blendingMode = OF_BLENDMODE_SUBTRACT;
                else if("multiply")
                    blendingMode = OF_BLENDMODE_MULTIPLY;
                else if("screen")
                    blendingMode = OF_BLENDMODE_SCREEN;
            }
        }
    }
    else if(isMatch(command, "smoothing"))
    {
        if(validateOscSignature("([TFifs])", m))
        {
            smoothing = getArgAsBoolUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "curveResolution"))
    {
        if(validateOscSignature("([if])", m))
        {
            curveResolution = getArgAsIntUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "sphereResolution"))
    {
        if(validateOscSignature("([if])", m))
        {
            sphereResolution = getArgAsIntUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "circleResolution"))
    {
        if(validateOscSignature("([if])", m))
        {
            circleResolution = getArgAsIntUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "lineWidth"))
    {
        if(validateOscSignature("([if])", m))
        {
            lineWidth = getArgAsFloatUnchecked(m, 0);
        }
    }

}
