#pragma once
#ifndef LAYER_H
#define LAYER_H

#include "Element.h"
#include "ObjectProvider.h"
#include "PluginProvider.h"

namespace vilain
{
/**
 * \addtogroup elements
 * \{
 */

/**
 * \class Layer
 * \brief
 * \copyright GNU Public License
 */
class Layer :
    public Element
{
public:
    /**
     * \brief Named constructor
     * @param _name Layer name that identify itself
     */
    Layer(std::string _name);

    /**
     * \brief Destructor
     */
    virtual ~Layer();

    /**
     * \brief
     */
    virtual void draw();

    /**
     * \brief OSC Message processing callback
     *
     * This is the list of OSC messages that can be interpreted by the processor:
     * - ../layerName/exampleToRemove ifs varInt varFloat varString
     *
     * \param command OSC command string associated with this ofxOscRouterNode
     * \param m Complete ofxOscMessage container, with arguments and OSC packet informations
     *
     * \todo Write documentation for each OSC command available
     */
    virtual void processOscCommand(const std::string& command, const ofxOscMessage& m);

    /**
     * \brief
     */
    virtual void update();

    /**
     * \brief Associate an existing object to this Layer
     * @param _object
     */
    void associateObject(const std::string& _objectName);

private:
    std::map<std::string, Object*> objects;
    ObjectProvider * objectsMgr = nullptr; ///< Global Object collection manager
    PluginProvider * pluginsMgr = nullptr; ///< Global Plugin collection manager

    /**
     * \brief
     */
    virtual void computePlugins();
};
/**
 * \}
 */
};
#endif // LAYER_H
