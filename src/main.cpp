#include "vilain.h"
#include "Core.h"

using namespace vilain;
/**
 * \defgroup core Core system
 * \{
 */

/**
 * \brief Main programm entry
 * \return An integer 0==x if all right, 0<x if something wrong
 * \copyright GNU Public License
 */
int main()
{
    setlocale(LC_ALL, "");
    bindtextdomain(PROG_NAME, ".");
    textdomain(PROG_NAME);
#ifdef DEBUG
    ofSetLogLevel(OF_LOG_VERBOSE);
#else
    ofSetLogLevel(PROG_NAME, OF_LOG_ERROR);
    ofSetLogLevel(OF_LOG_SILENT);
#endif
    ofGLWindowSettings settings;
    settings.title = PROG_NAME;
    settings.setGLVersion(3, 2);
    settings.width = 1024;
    settings.height = 768;
    settings.windowMode = ofWindowMode::OF_WINDOW;
    ofCreateWindow(settings);
    return ofRunApp(new Core());
}

/**
 * \}
 */
