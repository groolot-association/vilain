#pragma once
#ifndef CORE_H
#define CORE_H

#include "vilain.h"
#include "Configuration.h"
#include "SceneProvider.h"
#include "LayerProvider.h"
#include "ObjectProvider.h"
#include "PluginProvider.h"

#include <ofxOsc.h>
#include <ofxOscRouter.h>

namespace vilain
{
/**
 * \addtogroup core
 * \{
 */

/**
 * \class Core
 * \brief Core application class that contain all the relevant main algorithme
 * \copyright GNU Public License
 */
class Core :
    public ofBaseApp,
    public ofxOscRouter
{
public:
    /**
     * \brief Base constructor
     */
    Core();

    /**
     * \brief Base destructor
     */
    ~Core();

    /**
     * \brief Initial openFrameworks configuration setup
     */
    void setup();

    /**
     * \brief Updating mechanism, for animations or something like
     *
     * Called as soon as possible
     */
    void update();

    /**
     * \brief Drawing on screen or framebuffer
     *
     * Called every frame
     */
    void draw();

    /**
     * \brief Exiting callback
     */
    void exit();

    /**
     * \brief OSC Message processing callback
     *
     * This is the list of OSC messages that can be interpreted by the processor:
     * - /coreName/exampleToRemove ifs varInt varFloat varString
     *
     * \param command OSC command string associated with this ofxOscRouterNode
     * \param m Complete ofxOscMessage container, with arguments and OSC packet informations
     *
     * \todo Write documentation for each OSC command available
     */
    void processOscCommand(const std::string& command, const ofxOscMessage& m);

    /**
     * \brief Key pressed callback
     *
     * Called as soon as a keyboard key is pressed
     * @param key The key number
     * @see OF_KEY_ALT and followings
     */
    void keyPressed(int key);

    /**
     * \brief Key released callback
     *
     * Called as soon as a key is released
     * @param key The key number
     * @see OF_KEY_ALT and followings
     */
    void keyReleased(int key);

    /**
     * \brief Mouse moved callback
     *
     * Called as soon as the mouse has moved
     * @param x X coordinate of the mouse
     * @param y Y coordinate
     */
    void mouseMoved(int x, int y);

    /**
     * \brief Mouse pressed callback
     *
     * Called as soon as a mouse button is pressed
     * @param x X coordinate of the mouse
     * @param y Y coordinate
     * @param button Clicked button number
     */
    void mousePressed(int x, int y, int button);

    /**
     * \brief Mouse released callback
     *
     * Called as soon as a mouse button is released
     * @param x X coordinate of the mouse
     * @param y Y coordinate
     * @param button Released button number
     */
    void mouseReleased(int x, int y, int button);

    /**
     * \brief Mouse scrolled callback
     * @param x X coordinate of the mouse
     * @param y Y coordinate
     * @param scrollX X scroll offset
     * @param scrollY Y scroll offset
     */
    void mouseScrolled(int x, int y, float scrollX, float scrollY);

    /**
     * \brief
     * @param w
     * @param h
     */
    void windowResized(int w, int h);

private:
    ofxOscMessage oscMessage; ///< Class scope variable used to store the OSC message to be sent
    ofxOscSender oscSender; ///< OSC sneder mechanism
    bool bDebug; ///< Debug information flag
    bool bBlackOut; ///< Emergency black out rendering flag
    Configuration * configurationMgr = nullptr; ///< Configuration manager
    SceneProvider * scenesMgr = nullptr; ///< Scene collection manager
    LayerProvider * layersMgr = nullptr; ///< Layer collection manager
    ObjectProvider * objectsMgr = nullptr; ///< Object collection manager
    PluginProvider * pluginsMgr = nullptr; ///< Plugin collection manager
};

/**
 * \}
 */
};
#endif // CORE_H
