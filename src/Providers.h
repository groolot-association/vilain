#pragma once
#ifndef PROVIDERS_H
#define PROVIDERS_H

#include "vilain.h"
#include "ofxOscRouterNode.h"

#include <stdexcept>

/**
 * \defgroup providers Providers
 *
 * Providers that store collections
 *
 * \{
 */
namespace vilain
{
class SceneProvider;
class LayerProvider;
class ObjectProvider;
class PluginProvider;
};

using namespace vilain;

/**
 * \class Provider
 * \brief Template class that helps to create and manage a collection of Element
 *
 * Our Elements are mainly Scenes, Layers, Objects, Plugins, ...
 * \tparam ElementType The Element type that that Provider make a collection of
 * \copyright GNU Public License
 */
template<typename ElementType>
class Provider :
    public ofxOscRouterNode
{
    friend SceneProvider;
    friend LayerProvider;
    friend ObjectProvider;
    friend PluginProvider;

public:
    /**
     * \brief Create and insert an element inside the provider collection
     * Element will be identified by its name given as parameter
     * @param _elementName Provider and its OSC node string identifier
     * @return
     */
    ElementType* create(std::string _elementName)
    {
        ofLogVerbose(PROG_NAME, "Create a new " + ofToString(typeid(ElementType).name()) + " named '" + _elementName + "'");
        ElementType* newElement = new ElementType(_elementName);
        this->add(newElement);
        return newElement;
    };

    /**
     * \brief Add an existing element to the provider
     * The element should have been instanciated before or inside
     * function call parameter like example below:
     *
     * @code{.cpp}
     * ElementType* myElement = new ElementType("nodeName");
     * this.add(myElement);
     * @endcode
     *
     * or
     *
     * @code{.cpp}
     * this.add(new ElementType("nodeName"));
     * @endcode

     * @param _element Pointer to the element to be added
     *
     */
    void add(ElementType* _element)
    {
        std::string _elementName = _element->getNodeName();
        ofLogVerbose(PROG_NAME, "Add " + ofToString(typeid(ElementType).name()) + " element named " + _elementName + " to '" + name + "' provider");
        collection[_elementName] = _element;
        addOscChild(_element);
    };

    /**
     * \brief Remove an element from the provider
     * You have to notice that the element is removed
     * from the collection, not from the memory.
     * If you don't have any copy of the element reference
     * you are going to use space in memory for nothing.
     *
     * If you want to totally remove the element from the
     * entire program, you have to free memory before removing
     * it from the last provider.
     *
     * For example, a BAD one:
     * @code{.cpp}
     * ElementType* myElement = new ElementType("elementName");
     * Provider<ElementType> p;
     * p.add(myElement); // element is added to the provider
     * myElement = NULL; // lost of the reference
     * p.remove("elementName"); // element is removed, BUT memory is not freed
     * @endcode
     *
     * Now, a BETTER example:
     * @code{.cpp}
     * ElementType* myElement = new ElementType("elementName");
     * Provider<ElementType> p;
     * p.add(myElement); // element is added to the provider
     * myElement = NULL; // lost of the reference
     * myElement = p.getPtr("elementName");
     * p.remove("elementName"); // element is removed, BUT memory is not freed
     * delete myElement; // freeing memory
     * myElement = NULL; // null pointer
     * @endcode
     *
     *
     * @param _elementName Element's name to be removed from the collection
     * @return
     */
    bool remove(std::string _elementName)
    {
        if(collection.erase(_elementName) > 0)
        {
            ofLogVerbose(PROG_NAME, "\'" + _elementName + "\' element has been removed from \'" + name + "\' provider");
            return true;
        }
        else
            return false;
    };

    /**
     * \brief Get a plain object copy of the element identified by its name
     * @param _elementName Element identifier name
     * @return A copy of the element requested
     */
    ElementType get(std::string _elementName)
    {
        return (*getPtr(_elementName));
    };

    /**
     * \brief Alias operator to getPtr function
     * @param _elementName Element identifier name
     * @return
     */
    ElementType* operator [](std::string _elementName)
    {
        return getPtr(_elementName);
    };

    /**
     * \brief Get a reference pointer to the element identified by its name
     * If the element could not be reached, exception out_of_range is caught.
     *
     * @param _elementName Element identifier name
     * @return Pointer to element type ElementType corresponding to the element requested
     */
    ElementType* getPtr(std::string _elementName)
    {
        try
        {
            return collection.at(_elementName);
        }
        catch(const std::out_of_range& oor)
        {
            ofLogFatalError(PROG_NAME, "Access to element \'" + _elementName + "\' failed");
            ofLogFatalError(PROG_NAME, "Something went wrong. You should close the application right now, if it has not crashed yet!");
            ofLogFatalError(PROG_NAME, "Please concider sending a bug report to the main developper team.");
            ofLogFatalError(PROG_NAME, "You can do so by:");
            ofLogFatalError(PROG_NAME, "\topening an issue in project management system: http://framagit.org/groolot/vilain");
            ofLogFatalError(PROG_NAME, "\tor sending an \"vilain:: Fatal error\" titled email to: <dev@groolot.net>");
            ofLogFatalError(PROG_NAME, "Each report way is pleased to receive a copy of the output and reproduceable actions.");
            ofLogFatalError(PROG_NAME, "\nThanks to use " + ofToString(PROG_NAME));
        }

        return NULL;
    };

    /**
     * \brief Get a copy of the complete collection of elements stored by this Provider
     * @return A copied std::map<std::string, ElementType*> container with all managed elements
     */
    std::map<std::string, ElementType*> getCollection()
    {
        return collection;
    };

    /**
     * \brief Const variant of getCollection that do not copy collection
     * The collection is given by constant reference.
     *
     * @return A constant reference to the inner collection
     */
    const std::map<std::string, ElementType*>& getCollection() const
    {
        return collection;
    };

    /**
     * \brief Get the provider global and OSC node name
     * @return The std::string node name
     */
    const std::string& getNodeName()
    {
        return name;
    };

    /**
     * \brief Define the name of this provider node
     * Notice that the name have to comply with OSC 1.0+ address specifications.
     * @see http://opensoundcontrol.org/spec-1_0
     * @param _nodeName The node name to be set
     */
    void setNodeName(std::string _nodeName)
    {
        name = _nodeName;
        clearOscNodeAliases();
        addOscNodeAlias(name);
    };

    /**
     * \brief OSC Message processing callback
     * This is the list of OSC message that can be interpreted by the processor:
     * - ../layers/exampleToRemove ifs varInt varFloat varString
     *
     * \param command OSC command string associated with this ofxOscRouterNode
     * \param m Complete ofxOscMessage container, with arguments and OSC packet informations
     *
     * \todo Write documentation for each OSC command available
     */
    virtual void processOscCommand(const std::string& command, const ofxOscMessage& m)
    {
        ofLogVerbose(PROG_NAME, "Global provider class received an OSC command: " + command);
    };

private:
    /**
     * \brief Default constructor
     * Gives "provider" as standard OSC node name, that should be
     * overrided by child class constructor.
     */
    Provider() : Provider("provider") {};

    /**
     * \brief Named constructor
     * Intanciate the provider with the parameter as OSC node name
     *
     * @param _nodeName Provider and its OSC node string identifier
     */
    Provider(const std::string& _nodeName) : ofxOscRouterNode(_nodeName), name(_nodeName) {};

    /**
     * \brief Destroy the provider
     *
     */
    ~Provider()
    {
        clearOscNodeAliases();
    };

    std::string name;
    std::map<std::string, ElementType*> collection;
    Provider(const Provider& rhs);
    Provider& operator=(const Provider& rhs);
};
/**
 * \}
 */
#endif // PROVIDERS_H
