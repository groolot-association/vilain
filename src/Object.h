#pragma once
#ifndef OBJECT_H
#define OBJECT_H

#include "vilain.h"
#include "Element.h"
#include <ofxMeshWarp.h>
#include <ofxOscRouterNode.h>
#include <ofxAnimatableFloat.h>

namespace vilain
{
/**
 * \addtogroup elements
 * \{
 */

/**
 * \class Object
 * \brief An Object is the core Element for the projection mapping
 *
 * Its aim is to host a still image or a moving animation, modify
 * the content graphics with plugins, adapt texture coordinate and finaly
 * transform geometry to fit the wanted perspective.
 *
 * At last, Object is the container of the purpose to be mapped.
 * \copyright GNU Public License
 */
class Object :
    public Element
{
public:
    /**
     * \brief Named constructor
     * @param _name Object name that identify itself
     * @param _key Activation keyboard key (0 to disbale, default)
     */
    Object(std::string _name, unsigned char _key = 0);

    /**
     * \brief Destructor
     */
    virtual ~Object();

    /**
     * \brief OSC Message processing callback
     *
     * This is the list of OSC messages that can be interpreted by the processor:
     * - ../objectName/exampleToRemove ifs varInt varFloat varString
     *
     * \param command OSC command string associated with this ofxOscRouterNode
     * \param m Complete ofxOscMessage container, with arguments and OSC packet informations
     *
     * \todo Write documentation for each OSC command available
     */
    virtual void processOscCommand(const std::string& command, const ofxOscMessage& m);

    /**
     * \brief
     */
    virtual void update();

    /**
     * \brief
     */
    virtual void draw();

    /**
     * \brief
     */
    void computePlugins();

    /**
     * \brief
     * @param _maximumVisibility
     */
    virtual void show(float _maximumVisibility = 1);

    /**
     * \brief
     */
    virtual void hide();

    /**
     * \brief
     * @param _valAlpha
     */
    void Alpha(float _valAlpha);

    /**
     * \brief
     */
    float Alpha();

    /**
     * \brief
     */
    bool Drawable();

    /**
     * \brief
     * @param _val
     */
    void Drawable(bool _val);

    /**
     * \brief
     */
    void Toggle();

    /**
     * \brief
     * @param _val
     */
    void Animable(bool _val);

    /**
     * \brief
     */
    bool Animable();

    /**
     * \brief
     */
    const char Key();

    /**
     * \brief
     * @param _key
     */
    void Key(char _key);

protected:

private:
    /**
     * \brief
     */
    virtual void initializeOscMethods();

    bool bDrawable; ///< Object is drawn while True, not if False

    bool bAnimable; ///< Object is animable while True, not if False.
    ///< By the way, nothing automatic that can control movement or changing
    ///< are available, except toggling back object animable

    char activeKey; ///< The keyboard key that toggle bDrawable

    ofxAnimatableFloat alpha; ///< Alpha automatic controlable parameter
};
/**
 * \}
 */
};
#endif // OBJECT_H
